tailwind.config = {
    theme: {
      fontFamily: {
        sans: ['"Algerian"', 'sans-serif']
      },
      extend: {
        colors: {
          clifford: '#da373d',
          turquoise: '#8ef6e4',
          taro_purple: '#9896f1',
          sunset_purple: '#d59bf6',
          pinkish_purple: '#edb1f1'
        }
      }
    }
  }