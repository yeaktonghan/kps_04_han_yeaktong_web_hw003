
function addZero(time){
    if(time < 10){
        return time = "0" + time;
    }
    if (time >= 10){
        return time;
    }
}

    // const days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
    // let dayName = days[d.getDay()]
    // manual method to get String in date

function displayDate(){
    var d = new Date();

    let month = d.toLocaleString('default', { month: 'long' });
    let dayName = d.toLocaleString('default', { weekday: 'long'})
    let dayOfMonth = d.getDate();
    // getUTCDate actually return wrong date
    let year = d.getFullYear();
    var hour = d.getHours();
    hour = addZero(hour);
    var unConvertedHour = d.getHours();

    var minute = d.getMinutes();
    minute = addZero(minute);

    var second = d.getSeconds();
    second = addZero(second);

    var session = "AM";
    if (hour > 12){
        hour -= 12;
        session = "PM"
    } 

    if (hour == 12) {
        session = "PM"
    }

    if( hour == 0){
        hour = 12;
    }

    document.getElementById("date-time").innerHTML = `${dayName} ${month} ${dayOfMonth} ${year} ${hour}:${minute}:${second} ${session}`;
    setTimeout(displayDate, 1000);

    objDate = {
        getHours: unConvertedHour,
        getMinutes: minute,
        getSeconds: second,
        getSession: session,
    }
}

displayDate();

let btn = document.getElementById("button");

function clickBtn(){
    if(btn.value == "Start"){
        getStartTime();
        btn.className = "px-6 py-2 border-4 border-yellow-500 text-yellow-500 font-bold uppercase rounded-full hover:bg-black hover:bg-opacity-5"
        btn.innerHTML = "Stop";
        btn.value = "Stop";
    }else if (btn.value == "Stop"){
        getStopTime();
        getTotalUsageMinutes();
        displayTotalMinutes();
        getRate();
        btn.className = "px-6 py-2 border-4 border-red-500 text-red-500 font-bold uppercase rounded-full hover:bg-black hover:bg-opacity-5"
        btn.innerHTML = "Clear";
        btn.value = "Clear"
    }else if(btn.value=="Clear"){
        clearData();
        btn.className = "px-6 py-2 border-4 border-green-500 text-green-500 font-bold uppercase rounded-full hover:bg-black hover:bg-opacity-5"
        btn.innerHTML = "Start";
        btn.value = "Start";
    }
}


function getTotalUsageMinutes(){
    let usageStartHour = startTime.startHour;
    let usageStartMinute = startTime.startMinute;
    let usageStartSecond = startTime.startSecond;

    let usageStopHour = stopTime.endHour;
    let usageStopMinute = stopTime.endMinute;
    let usageStopSecond = stopTime.endSecond;


    // this is correct
    if(usageStopHour - usageStartHour > 0){
        usageStopMinute = ((usageStartHour - usageStartHour) * 60) + usageStopMinute;
    }

    // this is also correct
    if(usageStopSecond > usageStartSecond){
        usageStopMinute = (+usageStopMinute) + 1;
    }


    // this is correct
    totalMinutes = usageStopMinute - usageStartMinute;
    
    objTotalMinutes = {
        usageTotalMinutes: this.totalMinutes
    }
}

function clearData(){
    let startBox = document.getElementById("start").innerHTML = "Start at:";
    let stopBox = document.getElementById("stop").innerHTML = "Stop at:";
    let chargeBox = document.getElementById("billing").innerHTML = "Riels";
    let minuteBox = document.getElementById("total-usage").innerHTML = "Minutes(s)";

}

function getStartTime(){
    let start = document.getElementById("start").innerHTML = `Start at ${objDate.getHours}:${objDate.getMinutes}:${objDate.getSeconds} ${objDate.getSession}`
    // let start = document.getElementById("start").innerHTML = `07:10:45` 
    startTime = {
        startHour: objDate.getHours,
        startMinute: objDate.getMinutes,
        startSecond: objDate.getSeconds
    }
}

function getStopTime(){
    let start = document.getElementById("stop").innerHTML = `Stop at: ${objDate.getHours}:${objDate.getMinutes}:${objDate.getSeconds} ${objDate.getSession}`
    // let start = document.getElementById("start").innerHTML = `07:10:45` 
    stopTime = {
        endHour: objDate.getHours,
        endMinute: objDate.getMinutes,
        endSecond: objDate.getSeconds
    }
}

function displayTotalMinutes(){
    let minuteBox = document.getElementById("total-usage");
    console.log(objTotalMinutes.usageTotalMinutes)
    minuteBox.innerHTML = `${objTotalMinutes.usageTotalMinutes} minute(s)`;
}

function getRate(){
    let charge = 0;
    chargeMinute = objTotalMinutes.usageTotalMinutes;
    if (chargeMinute <= 15){
        charge = 500;
    } else if (chargeMinute <= 30){
        charge = 1000;
    } else if (chargeMinute > 30) {
        if (chargeMinute > 30 && chargeMinute < 60){
            charge = 1500;
        }else if (chargeMinute % 60 == 0){
            charge = (chargeMinute / 60) * 1500;
        } else if (chargeMinute % 60 <= 15){
            charge = (Math.trunc(chargeMinute / 60) * 1500) + 500; // Math.trunc(123/60) 
        } else if (chargeMinute % 60 <= 30){
            charge = (Math.trunc(chargeMinute / 60) * 1500) + 1000;
        } else if (chargeMinute % 60 > 30){
            charge = (Math.trunc(chargeMinute / 60) * 1500) + 1500;
        }
    }

    let chargeBox = document.getElementById("billing").innerHTML = `${charge} riel(s)`

    function objGetRate(){
        totalCharge = this.charge;
    }
}



// function calculateRate(){
//     startTime.hour - endTime.hour
// }
